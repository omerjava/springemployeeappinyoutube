package com.springEmployeeApp.service;

import com.springEmployeeApp.entity.Employee;
import com.springEmployeeApp.exception.EmployeeNotFoundException;
import com.springEmployeeApp.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public List<Employee> getAllEmployees() {
        List<Employee> employees = employeeRepository.findAll();
        return employees;
    }


    public Employee createEmployee(Employee employee) {
        Employee savedEmployee = employeeRepository.save(employee);
        return savedEmployee;
    }


    public Employee getEmployeeById(Long id) {
        Employee employeeFound = employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("There is no employee with this Id no"));
        return employeeFound;
    }


    public Employee updateEmployee(Employee employee, Long id) {
        Employee employeeToUpdate = getEmployeeById(id);

        if(employee.getEmail()!=null && !Objects.equals(employee.getEmail(), "")) employeeToUpdate.setEmail(employee.getEmail());
        if(employee.getName()!=null && !Objects.equals(employee.getName(), "")) employeeToUpdate.setName((employee.getName()));
        if(employee.getPosition()!=null&& !Objects.equals(employee.getPosition(), "")) employeeToUpdate.setPosition(employee.getPosition());

        Employee updatedEmployee = employeeRepository.save(employeeToUpdate);

        return updatedEmployee;
    }

    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }
}
